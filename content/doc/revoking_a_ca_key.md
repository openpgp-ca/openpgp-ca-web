+++
title = "Revocation certificates for a CA key"
keywords = ["OpenPGP CA", "tutorial", "revocation"]
weight = 75
[menu.main]
parent = "running-a-ca"
+++

<!--
SPDX-FileCopyrightText: 2019-2022 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# Why would a CA key need to be revoked?

In the (hopefully) unlikely case that the private key material of an OpenPGP 
CA instance is compromised, any cryptographic statements by that CA key are 
suspect. No one should rely on statements by that CA key anymore (because 
they could be forged by a third party).

To disseminate the information that the CA key has become untrustworthy, a 
*revocation certificate* needs to be published. A revocation certificate 
is a cryptographic statement that marks a key as irrevocably invalid.
A revocation can be published via any channel, including WKD and any of 
the public keyservers.

By publishing a revocation, end users (and their OpenPGP software) who might 
have configured a "trust signature" for the CA key on their system, learn 
that the CA key is now considered invalid - and that it should not be trusted 
anymore.

# Generating a set of revocation certificates for future use

Using the OpenPGP CA CLI tool, you can generate a set of revocation 
certificates:

`$ oca -d example.oca ca revocations -o ca-revocations.txt`

The result is a human-readable text file with a short introductory text, 
followed by the CA public key, and then a number of revocation 
certificates. 

The revocations come in pairs consisting of a "hard" revocation (to signal 
that the CA key has been compromised) and a "soft" revocation (to 
signal that the CA key has not been compromised, but should nevertheless 
be considered retired):

```
[...]

-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: Hard revocation (certificate compromised) (2030-08-30)

wsHeBCABCgCSBYJyGprPCRC3ZBS5fYda+0cUAAAAAAAeACBzYWx0QG5vdGF0aW9u
cy5zZXF1b2lhLXBncC5vcme0ZegLeiC/jrr1lrnUlFGEnCNWgquguM1kfuW7XpIi
PyIdAkNlcnRpZmljYXRlIGhhcyBiZWVuIGNvbXByb21pc2VkFiEEVpka33FwM3DZ
WdeGt2QUuX2HWvsAACd4EACelPgC2ClfSHR+0OsSKOAjswi0gtCK/rJhvKuiBX6j
1oHQS9bzF4qWCWwQQuCKflQBKw65eHI6NqtQNGlTgTdef7gS0J0mhyvV3qUjzfjb
kY1XdWat1DGQsRYTypVFh6QOjowp/D0u67+YjW34l9v33H201oilKQYFgpEAL2k+
NCVXhIAsXpovWZ0BgWJ0UWLgkO5omwgwL1Js/RyqOcWEnfBWSNJ8H5vdwh38N1It
xTpTSeIeHgHPVHAuICcAKFeQAwfKnrV66hMjcL6ECzI4fg5QQRIHR5aC/GyMG+QM
vfiVaEh7iajuR2yZ3PXVKbfMLNHf66vLpAMIllEMoSeJ7DpSYAQiOmnKDMT7glW4
F1MgmZLadDLrINkQlJzd+pb/1oApkYwuBPG2m0T7TqFtD+ZR2tze7w0gdQlj05dB
wllULj2osRvbMmWl6l9tLpq5/xEL8/Jxw3jWVlEiinwlAlqX3O8qdXE1frKBfI0s
+h+xF1ckTROdR1UGTxSXmKrPSNnLoAdG1yxEJAWHSKuVYFuI8D7k1Q6Ct9Aeop/f
2eTZcgPxfaqiX43Ss0Nnz/azrNwK3E1tYQogZ1aSMGzwLe5HxG4NorPNkRqBtP2W
sSH65ykNqHPUKl7cymVECZtBQ+KnEXnhO//DFpksfPAKnQT2iFc+VUB+jB+TRb/d
Og==
=yJEI
-----END PGP PUBLIC KEY BLOCK-----


-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: Soft revocation (certificate retired) (2030-08-30)

wsHRBCABCgCFBYJyGprPCRC3ZBS5fYda+0cUAAAAAAAeACBzYWx0QG5vdGF0aW9u
cy5zZXF1b2lhLXBncC5vcmfXc7Lyff11Kq8ofcSiM48lPi5YHAthiAzSCAqyZZs4
uxUdA0NlcnRpZmljYXRlIHJldGlyZWQWIQRWmRrfcXAzcNlZ14a3ZBS5fYda+wAA
Se4QAJixJ/a2+E1iSkyvOpFGd9/qiqyhYdJxXAQ+XrKiSD3xh3KPNPW2wSp7+pP0
aBfxUD09jBkD+1N7rmX8SsBR1JeXev1VTsKeA1zYgvK/VJfbCQfD+d2F18Ixffxw
vhEvibphphdwf8FP380kEixyF8dydqrhOwSid/xsIcDiW2xx78OEpVq8dyzJiuYB
bwV97Yp+nd/m4lTus/h3o+MvTxbKY8MyAeFeyiRZ3V/+BLVHjbNHa4E7RsiUVG6x
xH/3QDmragxN+T3Q8ym6VUEXQB1AOpD4SCW7aaIob6xLgItWuTejmKg+K4qvBVSC
nOsrIkHLvIZWyjtXW+V4HX9lGcE5GeM1UmFXMZrpvqPp9/S2yd8ZyuUpVVEHmzGu
h3m92O2uuUKrjOLiBQRQSMiTc6eHsPhukmR4nvz8SKg9PfoRu+CB4+jL21+Qtrbk
6rlWhs5xIryHFhBEEZrgXJll41BsCLCdhl57UbrDpLZVk9alBOe3EbZlXQ6hTdKc
3IncgTFe6V6M7EFJe+2n6L8sRyiAiAXAfpodK6T3lsODIgIB054p7NyMGirhzmIS
SRWz8/WnilHlj7EFm8QowTDVfk73I2bBuliLXLpUbie82ZeSb+vVSdvva9ExZ+NF
mVTehga5LSm5iQaVafGCljCfx5AXRMCmEpXGgbVEAIOGayk9
=0qY9
-----END PGP PUBLIC KEY BLOCK-----

[...]
```

The file contains many such pairs (121 to be precise)[^cardtouch], which
differ in their "creation date" value. The creation dates range from the
current date to approximately 10 years in the future (in 30 day increments).

[^cardtouch]: If you are running a CA backed by an OpenPGP card,
and your card requires touch confirmation, this means that you will need
to confirm very many cryptographic operations (242).
This works, but is tedious. You might want to create these revocations before
enabling touch confirmation on your card.

If, in the future, you need to revoke your CA key, but don't have access 
to the CA private key anymore, you can choose and publish the most 
appropriate revocation from the set of revocations in this file.

# Storing the revocation file

You should store this text file in a safe location to be able to revoke 
the CA key in case your CA's private key ever gets compromised or lost 
due to some accident. Having a suitable revocation certificate in such 
cases gives you valuable options to do damage control - and recover from 
a failure.

It's important that the revocation file is not available to potential 
attackers. However, in comparison, it is less crucial to keep the revocations 
safe than it is to keep the CA's private key material safe. An attacker 
can do decidedly more damage with the private key material.

The intended use case for storing a revocation file is when you consider it 
too hard to store a copy of the CA private key with the appropriate security 
precautions.
