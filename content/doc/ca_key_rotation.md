+++
title = "CA key rotation"
keywords = ["OpenPGP CA", "tutorial", "CA Key rotation"]
weight = 77
[menu.main]
parent = "running-a-ca"
+++

<!--
SPDX-FileCopyrightText: 2019-2022 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# CA key rotation

Under some circumstances, you may want to start over with a fresh CA key.
Maybe you want to issue a CA key with a stronger algorithm - or, key rotation would be necessary if your CA key
got compromised.

Note: A user-friendly mechanism for replacing the CA key is not implemented yet.
Currently, the CA key can only be replaced by starting over with a fresh CA database and re-importing user keys, or by
manually editing the (sqlite) CA database file.


# Re-certification of user keys

Once you have rotated your CA key, you may want to automatically re-issue certifications for your user's keys with
your new CA key. That is: to issue fresh certifications for all User IDs that your old CA key had certified.

This makes sense if you have a copy of your CA database where you're sure that all certifications
by your old CA key are legitimate (and have not, for example, been issued by an attacker who has compromised your old
CA key). This copy might be an off-site snapshot of your CA database, that pre-dates a breach.

Let's assume your CA database now contains a new CA key, and your user's keys.
However, the user's keys currently only have certifications from you old CA key - and you want to automatically issue
fresh certifications for all User IDs that your CA previously certified.

To do this, you need a copy of the *old* CA public key. Here, we assume we have a copy in a file `old-ca.pub`:

```
$ oca -d example.oca ca re-certify --public-old old-ca.pub
```

By default, the new certifications are valid for 365 days (you can supply a different validity period with
`--validity`)
