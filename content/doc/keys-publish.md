+++
title = "Publishing keys as WKD or Keylist"
keywords = ["publish keys", "WKD", "Keylist"]
weight = 50
[menu.main]
parent = "running-a-ca"
+++

<!--
SPDX-FileCopyrightText: 2019-2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

OpenPGP CA serves as a repository of user keys within our organization.
So when we want to publish OpenPGP keys for our organization, exporting
those keys from OpenPGP CA is a convenient possibility. 

There are several ways to publish keys:
- [Web Key Directory](https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/) (WKD),
- [GPG Sync](https://github.com/firstlookmedia/gpgsync/)-style [Keylist](https://datatracker.ietf.org/doc/draft-mccain-keylist/),
- Key server (internal or [public](https://keys.openpgp.org/)),
- LDAP.

Of these, OpenPGP CA currently automates exporting as WKD and Keylist.

# Publish keys as a WKD

## Initialize an OpenPGP CA instance with test users

If we have no OpenPGP CA instance at hand, we set up a fresh one and create
two users:

```
$ oca -d example.oca ca init --domain example.org softkey
$ oca -d example.oca user add --email alice@example.org --name "Alice Adams"
$ oca -d example.oca user add --email bob@example.org --name "Bob Baker"
```

## Exporting a WKD

Now that we have an OpenPGP CA instance with two users, we can export the
WKD data for our organization to the filesystem into, say, `/tmp/wkd/`:

`$ oca -d example.oca wkd export /tmp/wkd/`

To use this data as a WKD, you need to configure a web server to serve this
data from the correct domain. The directory structure on the web server must
conform to the WKD specification, and https must be set up.

## Transferring the WKD to a webserver

`/tmp/wkd` contains the full path as needed on the webserver (starting with `.well-known/openpgpkey`).

So you might copy it to your webserver as follows: 

`rsync --dry-run --recursive --delete /tmp/wkd/.well-known/openpgpkey/ www@example.org:public_html/.well-known/openpgpkey/`

We use `--delete` to delete stale data. To avoid accidentally deleting data, we've added the '--dry-run' option to the rsync command.
When you've checked that the output of this `rsync` run looks as expected, run
rsync again, but without the `--dry-run` parameter, to actually perform
the copy and delete operations.

## Testing WKD

When we've set up a WKD server, we can retrieve keys from it using an
OpenPGP client.

For example with GnuPG:

`$ gpg --auto-key-locate clear,nodefault,wkd --locate-key alice@example.org`

```
gpg: key 7B675240E4B0CCE7: public key "Alice Adams <alice@example.org>" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: marginals needed: 3  completes needed: 1  trust model: pgp
gpg: depth: 0  valid:   1  signed:   1  trust: 0-, 0q, 0n, 0m, 0f, 1u
gpg: depth: 1  valid:   1  signed:   1  trust: 0-, 0q, 0n, 0m, 1f, 0u
gpg: depth: 2  valid:   1  signed:   1  trust: 0-, 0q, 0n, 0m, 1f, 0u
gpg: depth: 3  valid:   1  signed:   0  trust: 1-, 0q, 0n, 0m, 0f, 0u
pub   rsa3072 2020-07-03 [SC] [expires: 2022-07-03]
      23242C0704403804899C1B927B675240E4B0CCE7
uid           [ unknown] alice@example.org
sub   rsa3072 2020-07-03 [E]
```

Or with Sequoia PGP:

`$ sq wkd get alice@example.org`

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: 2324 2C07 0440 3804 899C  1B92 7B67 5240 E4B0 CCE7
Comment: Alice Adams <alice@example.org>

mQGNBF7/OtYBDADTHg2jy5IvDtjtC0mmnCYH4Bm8cHbPmHAdQJPCno9zZt27Hnap
AKKZY6/GtGwsSL3baAO7Q1R77ZKUsdsBb/zJk0JDdZZkyfOkdr29TyqF+fRD2SnH
[...]
EY6Q6JGTM5T360icl1dvBz+y5o2qaMRkGq/OURThdzQdONsfKyHbPEeB5cU1I+Hg
cir41OUw3syshyngRQNj/5WNpDVB756BZaHXr+4Wu69YLNzJ7DZdOp3qlupqok2w
Ay6X
=3yea
-----END PGP PUBLIC KEY BLOCK-----
```

# Publishing a CA's data as a "Keylist"

The [Keylist format](https://code.firstlook.media/keylist-rfc-explainer) 
is a simple method to publish a (signed) list of OpenPGP key fingerprints.

Based on such a Keylist, members of an organization can automatically receive 
up-to-date versions of a curated set of OpenPGP keys from a keyserver. 
This can be automated with tools, such as
[GPG Sync](https://github.com/firstlookmedia/gpgsync).

The functionality of Keylist is a subset of OpenPGP CA. Both projects 
formalize a method for an authority to make statements about a set of keys.
The curated set of OpenPGP keys from an OpenPGP CA instance can be exported 
as a Keylist (signed by the CA) like this:

```
$ oca -d example.oca keylist export -p . --sig-uri https://example.org/keylist.sig 
```

This command exports a Keylist file (and an accompanying digital signature 
file) to the current directory `-p .`. The Keylist will contain the URI
`https://example.org/keylist.sig` to specify where its digital signature 
can be retrieved. The Keylist and accompanying signature file must be 
hosted on a webserver. The path where the digital signature file will be 
available must be configured with the `--sig-uri` parameter.

(Note:
Unfortunately, the OpenPGP component of Thunderbird78 does not currently 
implement "web of trust" based authentication of keys. So there is no 
direct way that Thunderbird78 users can benefit from certifications by an 
OpenPGP CA instance. However, there is an
[experimental tool](https://crates.io/crates/tb-openpgp-certs) to import 
keys into a Thunderbird78 keyring - and to set their acceptance 
appropriately - based on a Keylist.
So export/import of a set of OpenPGP keys via a Keylist can serve as a 
workaround until Thunderbird78+ supports standardized OpenPGP authentication 
based on OpenPGP certifications.)
