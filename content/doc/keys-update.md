+++
title = "Updating keys from WKD or a keyserver"
keywords = ["update keys", "WKD", "keyserver"]
weight = 45
[menu.main]
parent = "running-a-ca"
+++

<!--
SPDX-FileCopyrightText: 2019-2022 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

Once you are managing a number of user keys in your CA, some of your 
users will eventually update their keys. For example they might extend 
the expiration date, add a subkey, or they might revoke their key by 
adding a revocation certificate to it.

If your users publish such changes on a keyserver or via WKD, it is 
useful to obtain and merge these changes into your CA, so that your copy 
of their key is up-to-date. This is what the OpenPGP CA `update` 
functionality does.

# Getting updates from the keys.openpgp.org keyserver

You can instruct your CA instance to pull updates for all keys from the 
keys.openpgp.org keyserver:

```$ oca -d example.oca update keyserver```

This queries the keyserver at keys.openpgp.org for each key's fingerprint
and merges any new information into your copy of the key.

The output of the command shows which keys received updates, if any.

# Getting updates from WKD

Analogously, you can pull updates for all keys via WKD:

```$ oca -d example.oca update wkd```

Each User ID for each of your users' keys will get queried via WKD, any 
new information gets merged into your copy of that key.

The output of the command shows which keys received updates, if any.

# Possible problems and risks

These update mechanisms get the latest information from online sources by 
fingerprint (or User IDs in the case of WKD) for each user key in your CA.

Running these update operations produces load on your network and on the 
remote services. Also, you should consider that polling for updates could 
have privacy implications: you are giving an external service insights into 
which keys your CA is managing.

Finally, by pulling updates for keys from external sources, there is a 
risk of flooding. In practice, your user's keys on both WKD and
keys.openpgp.org are typically controlled by the user themselves, so a 
flooding attack by a third party seems implausible.
