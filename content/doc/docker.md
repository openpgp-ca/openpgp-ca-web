+++
title = "Usage in Docker"
keywords = ["OpenPGP CA", "tutorial", "docker", "container"]
weight = 80
[menu.main]
parent = "running-a-ca"
+++

<!--
SPDX-FileCopyrightText: 2019-2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

## Running in Docker

You can use OpenPGP CA in container systems (such as [Docker](https://en.wikipedia.org/wiki/Docker_(software)) or [podman](https://podman.io/)).
The project repository contains a build file at `/Dockerfile`.

With docker you can build the image by running:

```shell
$ docker build --tag oca ./
```

This command builds the image and tags it as `oca`. Once built, you can run the image as:

```shell
$ docker run --rm oca
```

You should see the help output.

(We're using `oca` like a command line tool here. We don't want to start it as a long-running container.
Therefore, we use the `--rm` parameter, which tells docker to "automatically remove the container when it exits".)

Running any `oca` command is easy, just add the command and any parameters at the end of the docker call, like so:

```shell
$ docker run --rm oca ca init --domain example.org softkey
```

However, when we're running OpenPGP CA in a container, the database does not normally persist between runs.

Inside the container, the database is kept in `/var/run/openpgp-ca/` by default.
To operate a persistent OpenPGP CA instance in docker, you can create and use a volume:

```shell
$ docker volume create oca_test
$ docker run --rm -v oca_test:/var/run/openpgp-ca/ oca ca init --domain example.org softkey
```

The CA database is stored on that volume, which can be reused with a series of ephemeral containers.

Let's delete this test volume again:
```shell
$ docker volume rm oca_test
```

An example centralized workflow that creates a CA and a user then looks like:

```shell
$ docker volume create oca_test
$ docker run --rm -v oca_test:/var/run/openpgp-ca/ oca ca init --domain example.org softkey
$ docker run --rm -v oca_test:/var/run/openpgp-ca/ oca user add --email alice@example.org --email a@example.org --name Alicia
$ docker run --rm -v oca_test:/var/run/openpgp-ca/ oca user add --email bob@example.org
$ docker run --rm -v oca_test:/var/run/openpgp-ca/ oca user list
```

For regular use you probably want to employ more automated tools like
[`docker-compose`](https://docs.docker.com/compose/).

## Container Registry

You can find pre-built
[container images on gitlab](https://gitlab.com/openpgp-ca/openpgp-ca/container_registry/).

The "latest" tag can be used from
[registry.gitlab.com/openpgp-ca/openpgp-ca:latest](registry.gitlab.com/openpgp-ca/openpgp-ca:latest)

## Example usage

First, let's create a volume to store the OpenPGP CA database (which
contains all the state of this OpenPGP CA instance)

```shell
$ docker volume create example_ca
```

Then we can run a temporary container that uses this
volume and the latest OpenPGP CA build from our GitLab CI (of course you
can always build your own image, if you prefer):

```shell
$ docker run --rm -v example_ca:/var/run/openpgp-ca/ \
   registry.gitlab.com/openpgp-ca/openpgp-ca:latest
```

To initialize an OpenPGP CA instance for the domain `example.org`, we run

```shell
$ docker run --rm -v example_ca:/var/run/openpgp-ca/ \
   registry.gitlab.com/openpgp-ca/openpgp-ca:latest \
   ca init --domain example.org softkey
```

We create a new user:

```shell
$ docker run --rm -v example_ca:/var/run/openpgp-ca/ \
   registry.gitlab.com/openpgp-ca/openpgp-ca:latest \
   user add --email alice@example.org --name "Alice Adams"
```

... and then inspect the user database:

```shell
$ docker run --rm -v example_ca:/var/run/openpgp-ca/ \
   registry.gitlab.com/openpgp-ca/openpgp-ca:latest \
   user list

OpenPGP certificate 4D3B3C810C5A1383967C48E74825DDCB02A64CCB
 User 'Alice Adams'
 Identities certified by this CA:
 - 'Alice Adams <alice@example.org>'
 Has trust-signed this CA
 No expiration is set
 1 revocations available
```
