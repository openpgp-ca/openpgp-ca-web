+++
title = "Changelog"
keywords = ["OpenPGP CA", "changelog"]
weight = 99
[menu.main]
parent = "running-a-ca"
+++

<!--
SPDX-FileCopyrightText: 2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# openpgp-ca-lib 0.12.1 [2023-02-14]

- [Fixes crashes](https://gitlab.com/openpgp-ca/openpgp-ca/-/issues/95)
  of oca's `wkd export` and `user list` commands when user keys in the
  database are considered invalid by Sequoia's
  [`StandardPolicy`](https://docs.rs/sequoia-openpgp/latest/sequoia_openpgp/policy/struct.StandardPolicy.html).

# OpenPGP CA 0.12 [2023-02-03]

- The binary was renamed from `openpgp-ca` to `oca`
- Support for [OpenPGP card](https://en.wikipedia.org/wiki/OpenPGP_card)
  devices as a [backend](/doc/ca-card-backend/) for the CA key was added
- The 'ca init' syntax was changed from `openpgp-ca ca init <domain>`
  to `oca ca init --domain <domain> softkey`
  (previously, software backed CA instances were the implicit default)