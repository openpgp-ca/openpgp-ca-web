+++
title = "Initializing a CA instance"
keywords = ["OpenPGP CA", "initialize"]
weight = 15
[menu.main]
parent = "running-a-ca"
+++

<!--
SPDX-FileCopyrightText: 2019-2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

Now that we have [installed](/doc/start) the OpenPGP CA software, we can explore how to use it.

One main aspect of initializing a CA instance is setting up the CA's OpenPGP key.
That key is used by the CA to issue certifications. Issuing reliable certifications
is a central purpose of operating a CA. Appropriate handling of the CA's key
is an important consideration.

Currently, two mechanisms for handling the CA's private key material are supported:

- Software-backed keys ("softkey"), and
- [OpenPGP card](https://en.wikipedia.org/wiki/OpenPGP_card)-backed keys.

In this chapter, we discuss initializing a CA that uses software-backed keys.
See ["The OpenPGP card backend"](/doc/ca-card-backend) for more details about
using OpenPGP card devices to handle the CA's private key material.

## Setting up a OpenPGP CA instance

To start, if we don't already have an instance of OpenPGP CA, we need to set up a
new one. We initialize a new OpenPGP CA instance for the domain (in this case,
we'll use `example.org`) and generate a new keypair for the OpenPGP CA admin:

```console
$ oca -d example.oca ca init --domain example.org softkey
```

By convention, the OpenPGP CA admin uses the email address `openpgp-ca@example.org`.
If possible, you should adhere to this convention so that it is easier for
users and software to discover the CA key for your organization.

### Export the OpenPGP CA's public key

Then we export the CA public key to the file `openpgp-ca.pub`,
for use on the user's machine:

```console
$ oca -d example.oca ca export > openpgp-ca.pub
```
