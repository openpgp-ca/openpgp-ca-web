+++
title = "Installation, getting started"
keywords = ["OpenPGP CA", "tutorial", "getting started"]
weight = 10
[menu.main]
parent = "running-a-ca"
+++

<!--
SPDX-FileCopyrightText: 2019-2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

After you've familiarized yourself with
[OpenPGP CA's concepts](/background/overview/), let's get started using it!

# Installation

First, we install OpenPGP CA (or, alternatively, you can run OpenPGP CA
[in Docker](/doc/docker/)).

As prerequisites, we need to install:

1. Rust and Cargo (see https://www.rust-lang.org/tools/install)

2. The C-dependencies of Sequoia PGP
   (see ["Building Sequoia"](https://gitlab.com/sequoia-pgp/sequoia))  
   (e.g. on Debian bookworm, you need at least
   `apt install rustc cargo clang pkg-config nettle-dev libssl-dev libsqlite3-dev`)

3. libpcsclite to build OpenPGP card support: `apt install libpcsclite-dev`
   (at runtime, `pcscd` is used, so if you want to use cards, that service must be running)

Then we build the latest OpenPGP CA version from the [crates.io registry](https://crates.io/crates/openpgp-ca):

```console
$ cargo install openpgp-ca
```

After a successful build, the resulting binary is available in `~/.cargo/bin/`.
You might want to add that directory to your `$PATH` (or make the `oca` binary otherwise easily accessible).

Now, you should be able to run OpenPGP CA as follows:

```console
$ oca --help
```


# Database

OpenPGP CA uses an SQLite database to keep track of its state.
This database is stored in a single file in the filesystem. This file
is the only element of the filesystem that OpenPGP CA  modifies - and it is
the only file that you need to backup to have a copy of the full state of your
OpenPGP CA instance.

To use OpenPGP CA, you need to specify where the database file is stored
(there is no default location). There are two methods to configure the
database file:

1. You can set the `OPENPGP_CA_DB` environment variable.
2. Alternatively, the parameter `-d` sets the database file explicitly (this
   overrides the environment variable).
   
We're going to use the latter method in our examples.

If the configured database file doesn't exist, it will be created
implicitly.

## Multiple instances

If you operate multiple instances of OpenPGP CA, you can easily use
separate SQLite files: you just need one file per instance. You can then switch between
instances by setting the `OPENPGP_CA_DB` environment variable to
point to the correct database file for each instance, or you can use the
explicit `-d` parameter.

## Offline instances, encryption

The OpenPGP CA database contains sensitive data: in particular, it
contains information about the users, it can contain revocation
certificates - and, most importantly, the private key of OpenPGP CA[^softkey].

[^softkey]: The private CA key is only stored in the CA database in
"softkey" mode. When using the OpenPGP card backend, the CA private key
is not stored in the CA database.

Because of this, the database file(s) needs to be protected. Depending on the
needs of your organization, this might mean storing the OpenPGP CA database
on encrypted storage, and/or running OpenPGP CA on an airgapped machine (that is,
one that is not connected to a network).  
OpenPGP CA's workflows have not yet been optimized for offline operation.
But, we intend to add support for this in the future.


# Help

If you're stuck, then you can use the "--help" option to get information about any command or subcommand, for instance:

```console
$ oca --help
```

or 

```console
$ oca user --help
```
`
or

```console
$ oca user import --help
```
