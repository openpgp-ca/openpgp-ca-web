+++
title = "Why OpenPGP CA?"
keywords = ["use cases"]
weight = 10
[menu.main]
parent = "about"
+++

<!--
SPDX-FileCopyrightText: 2019-2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

OpenPGP CA can be applied in a wide variety of scenarios.
It serves two main goals:

# Make your organization's keys easy to find

*TL;DR: OpenPGP CA helps you manage your organization's OpenPGP public keys.
You can easily publish the keys via WKD. This solves the problem of (automated) key discovery.*

If you use OpenPGP, you probably agree that it can be hard to find the OpenPGP
key for a communication partner.
To improve this situation, organizations can publish the keys of their members.

With OpenPGP CA, your organization can manage a repository of OpenPGP 
public keys - and easily publish those keys as a
[Web Key Directory (WKD)](https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/).

WKD is well-supported by common client software (including Thunderbird and GnuPG).
It is a standardized, federated scheme.
WKD is usually the best approach for organizations to distribute keys[^wkd].

[^wkd]: WKD is almost always preferable to running a local keyserver
(like [hagrid](https://gitlab.com/hagrid-keyserver/hagrid/)). 
Client software will not automatically find custom keyservers, but it will 
automatically use new WKD instances.

    WKD is easy to deploy on your existing infrastructure: all you need is 
control over your domain's DNS configuration and a webserver.


# Users have easy access to vetted keys for their main contacts

*TL;DR: Key management and checking fingerprints are hard.
This hinders the adoption and secure use of OpenPGP. OpenPGP CA largely
moves these responsibilities to an expert who is trusted by users.*

To reliably protect your communication, it is important that the
cryptographic keys of your communication partners are authenticated:
you must follow procedures to make sure you are using the correct keys.

Authentication guards against user errors as well as
[active attacks](https://en.wikipedia.org/wiki/Man-in-the-middle_attack).

In the OpenPGP space, the burden of authentication has traditionally been 
put on individual users. Users were asked to personally verify the OpenPGP
fingerprints for each communication partner.
This approach is arguably impractical for most users.

However, the OpenPGP standard defines powerful mechanisms that 
can be used to implement other authentication regimes.
In fact, OpenPGP's flexible (and inherently decentralized) approach
to authentication is one of its distinguishing features.

OpenPGP CA enables users to delegate the work of authentication to
trusted third parties. This way, users can rely on the benefits of strong
authentication, without needing to manually validate the keys of
their communication partners.
This way, authentication can be the norm, while reducing user effort.

The idea that each user should personally verify the OpenPGP keys for 
each communication partner is related to the idea that users 
should not rely on third parties for the security of their digital 
communication. However, most users' IT security already hinges crucially 
on the work of in-house IT specialists anyway.
In such contexts, asking individual users to perform authentication work
serves no meaningful purpose. It just complicates the use of OpenPGP.

OpenPGP CA makes OpenPGP easier and safer to use.
It makes authenticated communication the default expectation,
without relying on external third parties (whose goals 
might not be aligned with yours).

# Learn more

You can read more [about the concepts behind OpenPGP CA](/background/overview/).
Detailed guidance about running the tool can be found in the "Running a CA" menu.

OpenPGP CA is designed to integrate well with your existing infrastructure.
We currently offer a CLI tool and a REST service.

[Contact us](/contact/) to talk about how to best integrate OpenPGP CA with your 
infrastructure and workflows!
