+++
title = "Relying on a CA"
keywords = ["tutorial", "relying on a CA", "trusted introducer", "fsfe"]
weight = 10
[menu.main]
parent = "end-users"
+++

<!--
SPDX-FileCopyrightText: 2019-2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

If your organization is running an OpenPGP CA instance, or you are
communicating with people at an organization that runs an OpenPGP CA
instance, you probably want to *rely on that CA*.
That is: tell your PGP software to make use of the authentication work that
the CA performs.

# What is an OpenPGP CA instance

An OpenPGP CA (*certification authority*) instance is a system that makes 
statements about cryptographic identities. Specifically, it makes
statements of the following nature:

*This CA has verified and certifies that the OpenPGP key
B702503FBB24BDB1656270786CC91D1754643106 is connected to the identity
"Alice Adams \<alice@example.org\>".*

In technical terms, these statements by the CA are called *certifications*
(or *signatures*).

# Why should I configure my system to rely on a CA

When you configure your OpenPGP software (such as GnuPG) to rely on a
particular CA, you tell it to take statements by that CA into account.
Your email software can then, for example, automatically determine if you
have the correct OpenPGP key for `Alice Adams <alice@example.org>`, based
on the statements made by the CA. You don't need to personally verify 
Alice's fingerprint.

(In technical terms, the way you tell your OpenPGP software that it should
rely on a CA is to generate a so called *trust signature* for that CA's 
OpenPGP key)


## User experience without CA

Traditionally, if you want to communicate securely with `alice@example.org`
and `bob@example.org`, you need to follow some manual procedure to check 
that you have the correct OpenPGP keys for both of these users 
(e.g. talking to both Alice and Bob in person to compare fingerprints).

Whenever you start communicating with someone new (such as
`carol@example.org`), you *again* need to follow your key-checking procedure.
Likewise if one of your communication partners switches to a new key.

## User experience with CA

In contrast, imagine that `example.org` is an organization that runs an 
OpenPGP CA instance. All OpenPGP keys of members of the organization are 
managed and certified by their CA.

You can configure your OpenPGP software to use that CA - after performing 
*one* manual key verification to ensure that you have the correct key for 
the `example.org` CA.

Then, your software can automatically verify if keys from that 
organization are authenticated. You don't need to check the fingerprints 
of keys for `alice@example.org` and `bob@example.org`, because even though 
you personally have not validated Alice or Bob's keys, the CA has done so,
and you can rely on the CA's (cryptographically protected) statements 
about Alice and Bob's keys.

If you start communicating with a third person `carol@example.org`, your 
software can automatically find and verify that you have the correct key 
for Carol.

Analogously, if anyone at `example.org` starts using a new key, 
your software can automatically recognize if that new key is authenticated.

The bottom line is: you don't need to perform any manual steps to 
authenticate OpenPGP keys for an organization once you use their CA.
This greatly simplifies your communication with everyone at that 
organization, while at the same time giving you a high level of 
confidence that you're not making dangerous manual mistakes or falling 
prey to attacks in your handling of keys.

# Configuring your GnuPG software to rely on the FSFE CA

In this tutorial, we demonstrate how to configure your GnuPG environment to 
rely on the OpenPGP CA instance of the
[Free Software Foundation Europe](https://fsfe.org).

More specifically, we will configure your GnuPG environment to trust the 
FSFE CA to make statements about *User IDs* under `@fsfe.org`. That is, we 
will rely on statements that the FSFE CA makes about OpenPGP keys which 
are linked to email addresses such as `example@fsfe.org`.

## Acquiring the CA key

To use the FSFE CA, first we need to acquire its OpenPGP key.

Here, we'll use WKD (*web key directory*), but it doesn't matter how we 
get the CA's key (we could instead get it from a public keyserver, the 
organization's website or by email).

```
$ gpg --locate-external-keys --auto-key-locate wkd openpgp-ca@fsfe.org

gpg: key 0x699D9E7F63B40E79: public key "FSFE OpenPGP CA <openpgp-ca@fsfe.org>" imported
gpg: Total number processed: 1
gpg:               imported: 1
```

We'll double-check that we have are using the correct key for the FSFE CA 
in the next step, so we won't worry about that, here.


## Configuring the CA key as a trusted introducer

Next, we'll configure our GnuPG environment to rely on the FSFE CA key.

In OpenPGP terminology, we configure the CA to act as a *trusted 
introducer* on our system. Based on this configuration, our OpenPGP 
software knows that we want it to honor cryptographic statements (that is, 
certifications - or signatures) made by the CA.

When you delegate (some) authentication decisions to a CA, it is important to 
make sure that you are delegating this trust to the correct entity - and 
not for example to an impostor.

You can, for example, verify that the fingerprint for the FSFE CA, as used 
in the next step of this tutorial (`A30A C9F8 FA7A 883C 0060  3070 699D 9E7F 
63B4 0E79`) matches the fingerprint published on
[https://keys.fsfe.org/](https://keys.fsfe.org/).

Once you've verified this, you can proceed with the following procedure
(which instructs your GnuPG setup to rely on that key as a CA):

```
$ gpg --edit-key 'A30A C9F8 FA7A 883C 0060  3070 699D 9E7F 63B4 0E79'
gpg (GnuPG) 2.2.20; Copyright (C) 2020 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.


pub  rsa4096/0x699D9E7F63B40E79
     created: 2021-01-21  expires: never       usage: C
     trust: unknown       validity: unknown
sub  rsa4096/0xDC52E4C251E216DF
     created: 2021-01-21  expires: never       usage: S
[ unknown] (1). FSFE OpenPGP CA <openpgp-ca@fsfe.org>

gpg> tlsign

pub  rsa4096/0x699D9E7F63B40E79
     created: 2021-01-21  expires: never       usage: C
     trust: unknown       validity: unknown
 Primary key fingerprint: A30A C9F8 FA7A 883C 0060  3070 699D 9E7F 63B4 0E79

     FSFE OpenPGP CA <openpgp-ca@fsfe.org>

Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)

  1 = I trust marginally
  2 = I trust fully

Your selection? 2

Please enter the depth of this trust signature.
A depth greater than 1 allows the key you are signing to make
trust signatures on your behalf.

Your selection? 1

Please enter a domain to restrict this signature, or enter for none.

Your selection? fsfe.org

Are you sure that you want to sign this key with your
key "Alice <alice@example.org>" (0x123456789ABCDEF0)

The signature will be marked as non-exportable.

Really sign? (y/N) y

gpg> save
```

Note that at the end, GnuPG asks you to make sure that you're making the 
trust signature with the intended key. It tells us it is going to use our 
identity *"Alice \<alice@example.org\>" (0x123456789ABCDEF0)*.
If you have multiple keys - for example one personal key and one code 
signing key - you can explicitly specify which one to use with the 
`--local-user` parameter, to avoid potential mistakes.

Also note that you have limited the delegation of trust to the domain 
`fsfe.org`. This means that your GnuPG environment will only accept 
certifications on User IDs of the form `foo@fsfe.org` from the FSFE CA.

This pattern of limiting the delegation of trust to a specific domain name 
is often appropriate. However, in some situations you will want to 
delegate trust to a CA without such a limitation (if your organization 
runs a CA instance, it may offer useful certifications for OpenPGP 
identities that are external to your organization's domain name).


## Verifying that the CA is now in use

Once we've performed this step, we can verify that GnuPG now considers 
the FSFE CA key `full`y trusted:

```
$ gpg -k openpgp-ca@fsfe.org
pub   rsa4096/0x699D9E7F63B40E79 2021-01-21 [C]
      Key fingerprint = A30A C9F8 FA7A 883C 0060  3070 699D 9E7F 63B4 0E79
uid                   [  full  ] FSFE OpenPGP CA <openpgp-ca@fsfe.org>
sub   rsa4096/0xDC52E4C251E216DF 2021-01-21 [S]
```

Make sure that GnuPG shows `[  full  ]` trust for the User ID
`FSFE OpenPGP CA <openpgp-ca@fsfe.org>`.


## Relying on the FSFE CA for authentication

Now that you have configured the FSFE CA to be recognized as a CA for
`fsfe.org` on your system, when you import other keys from the FSFE WKD, 
you'll see that they automatically show up as authenticated in your 
environment (note that the User ID `mk@fsfe.org` immediately shows 
`full` trust):

```
$ gpg --locate-external-keys --auto-key-locate wkd mk@fsfe.org

gpg: key A0616A85CE41AD88: public key "Matthias Kirschner <mk@fsfe.org>" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: marginals needed: 3  completes needed: 1  trust model: classic
gpg: depth: 0  valid:   6  signed:   8  trust: 0-, 0q, 0n, 0m, 0f, 6u
gpg: depth: 1  valid:   8  signed:   0  trust: 8-, 0q, 0n, 0m, 0f, 0u
gpg: next trustdb check due at 2021-04-11
pub   rsa4096 2014-01-04 [SC] [expires: 2023-01-05]
      C922F488F932F95D6F7C10F7A0616A85CE41AD88
uid           [ full] Matthias Kirschner <mk@fsfe.org>
sub   rsa4096 2014-01-04 [E] [expires: 2023-01-05]
```

Your OpenPGP software can recognize if keys are properly authenticated via 
the FSFE CA independent of how you acquired them. We use WKD here, but 
that's not necessary. You could acquire a key for "mk@fsfe.org" by any 
mechanism, including from non-validating public keyservers. Your GnuPG 
software can now automatically determine if a key that claims an identity 
at `fsfe.org` has been certified by the FSFE CA.


# Some more thoughts on security tradeoffs

The FSFE CA is an automated system that is connected to the public 
internet and makes certifications on cryptographic identities - much like 
the well known [Let's Encrypt](https://letsencrypt.org/) TLS certificate 
authority.

The certifications by the FSFE CA do not aim to represent any kind of 
"ultimate truth". Rather, the certifications show that an FSFE community 
member was able to log into the FSFE frontend and uploaded their OpenPGP 
key there (so the reliability of FSFE CA certifications roughly 
corresponds to the level of security of the FSFE's community portal).

That said, keys that are certified by the FSFE CA have much stronger 
authentication properties compared to keys that were obtained by querying 
a non-validating keyserver (such as the SKS pool) for a given email address.

Depending on your security needs, relying on certifications by the FSFE CA 
may be more than enough - or you might consider them insufficient for full 
confidence. Either way, you can use certifications by the FSFE CA as one 
signal. As a consumer of that signal, you can decide how much weight to 
give assign to it. For example, you could alternatively set your trust 
signature for the FSFE CA key to only `marginal` trust, and thus not 
consider certifications by the FSFE CA sufficient to fully authenticate keys.


# Potential pitfall: the GnuPG "trust model" setting

Especially if your GnuPG environment is old, it's possible that support for 
trust signatures is (presumably accidentally) disabled.
To see if this is the case, run `gpg --check-trustdb`. If that shows
`trust model: classic`, then trusted introducers are disabled in your setup:

```
$ gpg --check-trustdb
gpg: marginals needed: 3  completes needed: 1  trust model: classic
gpg: depth: 0  valid:   6  signed:   9  trust: 0-, 0q, 0n, 0m, 0f, 6u
gpg: depth: 1  valid:   9  signed:   2  trust: 9-, 0q, 0n, 0m, 0f, 0u
gpg: next trustdb check due at 2021-04-11
```

To use trusted introducers (which is necessary to take advantage of the 
certifications that OpenPGP CA makes), you need to make sure the
[GnuPG "trust model"](https://www.gnupg.org/documentation/manuals/gnupg/GPG-Configuration-Options.html#index-trust_002dmodel)
setting is configured to support that feature.

GnuPG offers two suitable "trust models": the current default GnuPG trust 
model `pgp`, or `tofu+pgp`. You should configure your GnuPG environment to 
use one of those.

There are two mechanisms by which the trust model can be configured in 
GnuPG:

1) The `~/.gnupg/gpg.conf` configuration file, which you can edit directly.

2) A setting within `trustdb.gpg`, which you can change by calling 
   e.g. `gpg --check-trustdb --trust-model=pgp`.
