+++
title = "Overview"
keywords = ["OpenPGP CA", "overview", "paradigm", "concepts"]
weight = 20
[menu.main]
parent = "about"
+++

<!--
SPDX-FileCopyrightText: 2019-2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# OpenPGP CA: a high-level overview

OpenPGP CA has two aspects. It is both:

- A **paradigm** (a set of best practices) for the use of OpenPGP in
  organizations or groups.
 
- Concrete **tooling** that helps apply this paradigm in practice.

This text focuses on the first aspect. We discuss the approach that OpenPGP CA
proposes, independent of concrete software tools[^proton].

[^proton]: The OpenPGP CA project provides a set of tools. However, our
central interest is to see more use of authentication in the larger
OpenPGP space, independent of the concrete software that is used.

    So we are very excited that Proton Mail
[launched an implementation](https://proton.me/blog/why-we-created-protonca)
of the OpenPGP CA paradigm for their email service in October 2022.

## Concepts

The OpenPGP CA paradigm touches on a number of concepts:

- OpenPGP
- Authentication
- Key discovery
- User key revocation

We'll briefly introduce them, next:

### OpenPGP

OpenPGP is an established standard for
[encryption and digital signing](https://en.wikipedia.org/wiki/Pretty_Good_Privacy#OpenPGP).
It is often used with email[^smime].

[^smime]: The other widely used email encryption standard, which OpenPGP CA does
not deal with, is [S/MIME](https://en.wikipedia.org/wiki/S/MIME).

There are [many implementations](https://www.openpgp.org/software/developer/)
of OpenPGP. All of them interoperate based on a
[shared standard](https://tools.ietf.org/html/rfc4880).

One very well known implementation is [GnuPG](https://gnupg.org/).
Thunderbird [directly supports OpenPGP](https://support.mozilla.org/en-US/kb/openpgp-thunderbird-howto-and-faq)
using the [RNP](https://www.rnpgp.org/) implementation[^tb].
Other implementations include [OpenPGP.js](https://openpgpjs.org/)
and [Sequoia PGP](https://sequoia-pgp.org) (OpenPGP CA uses Sequoia PGP internally).

[^tb]: Direct OpenPGP support in Thunderbird, based on RNP, was introduced
in version 78.
Before that, the [Enigmail](https://en.wikipedia.org/wiki/Enigmail) plugin
provided OpenPGP support for Thunderbird.

OpenPGP CA works seamlessly with existing OpenPGP software.

### Authentication

One of the most important aspects of secure communication is making sure that
your messages go to the right person and that messages you receive actually
came from the person they claim to.
This is [authentication](https://en.wikipedia.org/wiki/Authentication).

For many use cases, it's indispensable to establish authentication before
using encryption. Using encryption on an unauthenticated communication channel
is risky - we can't tell if our communication is secure.

Authentication is the core of what OpenPGP CA deals with.
The OpenPGP CA paradigm greatly simplifies authentication for users and makes it
commonplace.

### Key discovery

To start secure communication with a new person, you need to have a copy of
their public key. The process[^kd] of finding that key is called
"key discovery".

[^kd]: Key discovery can be a manual or an automated process.
Automated key discovery is more convenient for users.

OpenPGP CA facilitates key discovery, by making it easy for the admin to
publish keys in places that OpenPGP software normally looks for them (e.g.
[WKD](https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/)
and key servers). And, it makes it easier to keep those directories up to
date.

### Revocation of user keys

Finally, OpenPGP CA simplifies managing keys by (optionally) storing revocation
certificates for your users, and making it easy to publish a revocation, if
and when that is necessary.[^rev]

[^rev]: This style of "revocation escrow" can be useful when
a user loses access to their private key material.
It is often in the user's and the organization's best interest to revoke
that key, then (the revocation signals to third parties that they should
not use that key anymore).

    Centrally managing revocations is of course not appropriate in all
contexts.


## The need for OpenPGP CA, or: how OpenPGP gets introduced to organizations

Anecdotal evidence suggests that OpenPGP is often introduced to an
organization by a technically competent person who recognizes a need for
secure communication. That person then convinces everyone to adopt OpenPGP.
They organize a briefing (which is sometimes called a CryptoParty), and teach
everyone how to generate a key, how to do key discovery, how to certify keys,
etc. When done well, the members of the organization learn what they need to
do to communicate securely.

But, even in the best cases, most users often only have a superficial
understanding of what they need to do, and the technical leader needs to
provide them with intense support. And, due to the high overhead,
authentication is often neglected based on the perception that
"usually we will find the correct key in some kind of ad-hoc manner, anyway."

## OpenPGP CA and authentication 

The overarching goal of OpenPGP CA is that users can be certain they are using
the correct OpenPGP keys for their communication partners. This is called
authentication:
making sure that a key is indeed the correct key for the other party.

Imagine that we're receiving an email from Alice. Our email software can
indicate to us whether we can authenticate Alice's OpenPGP key.

For example, the Enigmail plug-in for Thunderbird 68 shows the authentication
status for a signed email with two visual cues:

1) the color of the bar at the top of the email window, and
2) the envelope icon on the right side of the window.

This is how an email looks from an unauthenticated sender:

{{< figure src="../../book/enigmail-sig.png"
width=100% alt="Thunderbird/Enigmail showing an unauthenticated signature" >}}

In contrast, when authentication works, Thunderbird/Enigmail shows this with a
green bar, and a wax seal on the envelope icon, both indicating that we have
an authenticated key for Alice:

{{< figure src="../../book/enigmail-sig-auth.png"
width=100% alt="Thunderbird/Enigmail showing an authenticated signature" >}}

More precisely, what is authenticated is the association between an identity
and a key. Authentication deals with identities and asserts that there is
reason to believe that a key in question is indeed controlled by a certain
identity.

If we want to be sure that an identity associated with a key is correct, we
need to perform steps to authenticate that key. A traditional method to
authenticate a key with OpenPGP is to have the owner (here: Alice) communicate the
fingerprint of that key. We might get it in person - asking the owner to
confirm that the fingerprint we have is the correct one. Or, the person might
have given us a business card that includes their fingerprint. These
approaches are time-consuming, and often not possible (what if you never meet
the person in real life?). OpenPGP CA simplifies authentication by
non-exclusively delegating that task to a trusted party.

### The importance of authentication

When we obtain an OpenPGP key that claims to belong to Alice from an untrusted
source, we cannot be sure if the key is indeed controlled by Alice, and not by
some malicious party.

This is a problem, because a malicious party could perform various types of
attacks, including:

- send us a seemingly correctly signed email (which might lead to false
  confidence in the origin of a piece of email - such attacks are
  called [phishing](https://en.wikipedia.org/wiki/Phishing)
  or [spear-phishing](https://en.wikipedia.org/wiki/Phishing#Spear_phishing)),
  or

- intercepting our attempts to securely communicate with Alice through an
  encrypted channel
  (["man-in-the-middle attack"](https://en.wikipedia.org/wiki/Man-in-the-middle_attack)).

### Records of authentication

OpenPGP CA introduces tooling that an admin can use to record organizational
knowledge in a form that OpenPGP software understands[^tps].

[^tps]: In technical terms, OpenPGP CA creates "third party certifications".
Those are often referred to colloquially as "signatures", in the OpenPGP
context.

    Third party certifications are machine-readable statements (they are, in
turn, also cryptographically secured: the issuer - in our case, a CA - commits
to the certification by signing it with their private key).
The OpenPGP standard defines these certifications, all compliant
implementations understand them.

Such a record makes a connection between an identity and a public key:

- Alice Adams' email address is `alice@example.org` (an identity)
- "This is a copy of Alice's OpenPGP public key".

## Giving users automatic authentication with OpenPGP CA

We can classify communication partners into three categories:

- people in the same organization,
- people in an affiliated organization, and
- others.
 
We consider two organizations to be affiliated if they often
communicate with each other. OpenPGP CA is designed to secure the first two
types of communication, but it can even help with the third type.

The following figure shows two affiliated organizations, `alpha.org`
and `beta.org`, a few of their members, a third-party (Carol). The arrows show
who communicates with whom. This communication pattern is typical for many
organizations.

{{< figure src="../../book/communication-1.png"
width=100% alt="Patterns of email between users of two organizations" >}}

Using OpenPGP CA, the admin of `alpha.org` and the admin of `beta.org` can
ensure that all of their users' OpenPGP-based tools can automatically
authenticate their main communication partners.
To do this, the CA admins authenticate user keys in their organizations
and distribute the information that they have done so.
The blue icons represent records of the CA admins' verification:

{{< figure src="../../book/communication-2.png"
width=100% alt="In-house CAs make certifications for their internal users' keys" >}}

Now, after the initial setup[^setup], normal users don't manually
authenticate (most of) their communication partners.
The admins do that work for them, with the help of OpenPGP CA.

[^setup]: We're glossing over the details of how that "initial setup" looks,
here. See the section on bridging, further down, for one possible approach.

    Putting aside details: each organizations' admins can set up user
OpenPGP environments to rely on the appropriate CA(s). Or give concrete
guidance how users should configure their systems.

## Intended Audience

OpenPGP CA helps organizations or groups model trust relations in OpenPGP.

We use the term "organization" to refer to any group of people who might use
OpenPGP CA together - whether they are an informal group, a commercial
organization, or any other form - such as:

- Journalistic organizations
- Human rights groups
- Informal groups of activists
- A company or a department at a company

OpenPGP CA's approach is based on the observation that in organizations, users
already trust their administrators with access to their data
(including email) and the integrity of their systems. So, asking the
administrator to also maintain a directory of users and keys does not
require placing additional trust in them.

## Users need to learn fewer skills

Implementing any new security practice requires educating users about its
operational aspects. The difficulty is making sure users are not overwhelmed,
as that undermines the security goals.

Traditionally, users of OpenPGP have had to learn many technical details.
These details are necessary to achieve the security goals. OpenPGP CA doesn't
remove these details, but shifts them to a new actor, the OpenPGP CA admin.
This greatly lessens the burden placed on normal users, who are more
interested in getting their job done than understanding technical details.

Concretely, OpenPGP CA users only need to be taught a few high-level skills to
get significant value from OpenPGP:

- encrypt and sign email, as well as
- how to recognize encrypted and signed email.

OpenPGP CA alleviates non-technical users from having to learn many low-level
tasks, such as:

- doing key discovery,
- checking that a key belongs to the intended party (authentication and
  certification), and
- creating their own key.

This allows users to focus on the indispensable high-level concerns and
reduces the risk of user errors.

This setup has several advantages for users. By delegating authentication to
an in-house CA, the cognitive burden placed on users is significantly reduced:
users don't have to compare fingerprints or even understand keys or
certifications to effectively use OpenPGP. At the same time, because OpenPGP
CA uses OpenPGP's existing mechanisms, users with special needs are free to
use other approaches in their OpenPGP environments, e.g., directly verifying
someone's key in the customary manner.

## The OpenPGP CA admin role

### What is the CA admin role?

OpenPGP has traditionally been aimed at individual users, each fending for
themselves. Our approach introduces the role of an "OpenPGP CA admin", a
per-organization role that serves as a centralized facilitator for OpenPGP
users in that organization.

The OpenPGP CA admin authenticates users within an organization, so that users
within that organization are mutually authenticated. Users can use each
others' OpenPGP keys with confidence - without the need to spend effort on
low-level details about key discovery and authentication.

The key assumption for OpenPGP CA is that users have a common trusted party
who can perform this OpenPGP CA admin role for them.

In formal organizations, this might be the existing OPSEC team. In an informal
group it might be a person who volunteers for that job and is viewed by the
other team members as both technically competent and trustworthy.

### Trade-offs of centralizing responsibility for authentication

Users need to trust their OpenPGP CA admin to do OpenPGP authentication on
their behalf. This effectively centralizes authentication, which deviates from
OpenPGP's decentralized tradition. However, contrary to TLS where a
third party customarily does the authentication, users in an organization
already trust their administrator.

In many cases, the OpenPGP CA admin role will be filled by the users' system
administrator. In that role, the administrator already controls the users'
systems - and can install arbitrary software, for instance.

In other words, such an administrator is already in a position where they
could undermine the integrity of users' systems in practically limitless ways.

Therefore trusting this admin to also authenticate business relationships does
not change existing trust boundaries - and does not increase the
organization's risk.

In short, the centralization that the OpenPGP CA admin role brings is not a
significant change to users' threat models, and does not undermine OpenPGP's
decentralized nature.

### Risks when a CA admin makes mistakes

The role of the CA admin centralizes tasks that would otherwise be the
responsibility of individual users. As described above, this brings a lot of
benefits for users, however, it also introduces new, centralized risks.

One particular difficulty in deploying OpenPGP CA is that the CA's key
needs to be well protected. If it is compromised, then an attacker is able to
mark arbitrary keys as authenticated.

Apart from that, when authenticating keys, the CA admin needs to be diligent.
The CA admin needs to have a good understanding of OpenPGP concepts to be able to
perform their tasks correctly. If the CA admin makes mistakes, users are at
risk. Depending on the consequences, not using OpenPGP CA may be better than
creating a false sense of security.

### Comparing the OpenPGP CA admin role with CAs for TLS server certificates

The concept of a certificate authority (CA) is well known in the context of
[TLS server certificates](https://en.wikipedia.org/wiki/Public_key_certificate#TLS/SSL_server_certificate),
which are used for authenticated and encrypted communication on the web.

TLS certificates claim an identity (the domain name of a website). This
identity is verified and cryptographically vouched for by a CA. CAs for TLS
server certificates act as a group of globally trusted verifiers of identity
(often, but not always, for-profit). To organizations that use TLS
certificates, these CAs are external actors.

While TLS server certificates of course bring massive benefits for secure
communication on the web, there are also problems:

- The interests of CAs in the TLS space are not aligned with the interests of
  their users.
- For-profit CAs are driven by profit, not by the needs of users.
- Law enforcement might interact with these CAs in ways that are contrary
  (and opaque) to the users of the certificates and detrimental to their
  goals. CAs agree to help, because they don't want to cause trouble with law
  enforcement.

These risks are real. TLS CAs have been known to be
[sloppy](https://en.wikipedia.org/wiki/NortonLifeLock#Google_and_Symantec_clash_on_website_security_checks)
and [potentially malicious](https://en.wikipedia.org/wiki/China_Internet_Network_Information_Center#Fraudulent_certificates).
And, the authentication that the CAs perform is often extremely weak.

Our main observation about TLS in this context is that it centralizes trust in
untrustworthy third parties.

### Interests of the OpenPGP CA admin and their users are aligned

In contrast, OpenPGP CA facilitates a decentralized (and optionally federated)
approach to trust and authentication.
OpenPGP CA embraces OpenPGP's decentralized trust model and makes it easy
to leverage its inherent benefits.

Trust management with OpenPGP CA is somewhat centralized - but only
*within* the scope of individual organizations. The interests of the in-house
CA admin are strongly aligned with the interests of the users and the
organization overall.

Using OpenPGP CA does not require placing trust in an outside third party with
potentially conflicting interests. This is essential for activists,
journalists, and lawyers.

## Federation: Bridges between organizations

OpenPGP CA cannot only be used to create authenticated paths between users in
a single organization, but also between organizations, in a decentralized,
federated manner.

{{< figure src="../../book/communication-3.png"
width=100% alt="Two organizations that configured bridging between each other" >}}

Affiliated organizations that use OpenPGP CA can easily set up what we call a
bridge between their respective CAs. Creating a bridge creates authenticated
paths between all users in the two organizations - users in each organization
can authenticate keys of users in the other without any additional effort.

This makes sense when the CA admins believe that the other organization's CA
admin does a good job authenticating their user's keys in OpenPGP CA.

Again, we want to take advantage of existing trust boundaries. When an organization
creates a bridge to another organization, the latter organization's CA admin
is only trusted to authenticate users in their own organization. This is done
by scoping the trust: when the CA admin from organization A authenticates the
CA admin from organization B, they indicate that B should only be trusted to
authenticate users in the same organization. This is done using existing
OpenPGP mechanisms. Specifically, by matching on the email's domain name.

## No key escrow

Centrally storing users' private key material has legitimate uses - however,
it also comes with massive risks.

OpenPGP CA does not do key escrow - that is, even when OpenPGP CA generates
keys for users, the private key material is never stored in the OpenPGP CA
database.

## Complements existing OpenPGP usage

Because OpenPGP CA works within the existing OpenPGP framework, i.e., it
doesn't require any modification of existing OpenPGP software, users do not
need any new software to take advantage of OpenPGP CA. Concretely, they can
continue to use existing email clients and encryption plugins. Further,
OpenPGP CA can co-exist with other authentication approaches like traditional
key certification workflows.

OpenPGP CA can be rolled out gradually within an organization.

While OpenPGP CA prescribes some aspects of how OpenPGP keys and
certifications should be handled, our approach
offers [some degrees of freedom](/doc/tradeoffs/), so that security
specialists in organizations can tailor their use to their specific needs.
That said, our framework/paradigm makes OpenPGP key management less ad-hoc and
more systematic.

# OpenPGP CA as tooling for the admin

OpenPGP CA vastly improves the capabilities of the administrator by giving
them tooling to model *existing* trust relationships in their organization.

Traditionally, tooling in the OpenPGP ecosystem was mainly aimed at users.
Dedicated administrators of OpenPGP setups have built ad-hoc tools for tasks
they perform, but these were point solutions, and often incomplete.

The benefits of OpenPGP CA are - at least in theory - not particular to
OpenPGP CA: a dedicated administrator could perform all the relevant tasks
manually. However, in practice, these tasks are so hard for administrators to
perform correctly with existing tooling that only very dedicated organizations
use this type of setup.

# Is OpenPGP CA suitable for my use case?

OpenPGP CA is not for everyone. Here are some considerations:

- OpenPGP CA is for groups. An individual will not profit from installing
  OpenPGP CA, because they have no one to delegate authentication to.
- There has to be a trusted, competent party who can perform the OpenPGP CA
  admin role.  
  The CA admin needs to have the relevant knowledge and skill set.
  Users must trust the admin to do authentication on their behalf.
- OpenPGP CA exposes the structure of the organization. For most companies,
  this is not a problem, because they already have employee directories. For
  some activist groups, this may be a problem.
