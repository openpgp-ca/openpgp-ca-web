+++
title = "Technical Details"
keywords = ["OpenPGP CA", "technical details"]
weight = 30
[menu.main]
parent = "about"
+++

<!--
SPDX-FileCopyrightText: 2019-2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

In the [previous chapter](/background/overview), we've discussed the
paradigm and concepts of OpenPGP CA at a high level.

Now, we'll take a more detailed, technical look at the relevant OpenPGP
mechanisms, as well as the OpenPGP CA software itself.

# The OpenPGP CA software

- OpenPGP CA is a suite of software, written in Rust.
  - It uses [Sequoia PGP](https://sequoia-pgp.org/).
  - All [code](https://gitlab.com/openpgp-ca/openpgp-ca) is Free Software
    under the GPLv3.
- The CA's private key can be used via different mechanisms:
  - directly as a "softkey" (the private key material is stored in the CA
    database and accessible to the computer that OpenPGP CA runs on), or 
  - using an [OpenPGP card](https://en.wikipedia.org/wiki/OpenPGP_card) device
    (this protects the CA key from exfiltration, and even from use by attackers,
    depending on the configuration).
- The architecture follows a "library first" approach:
  - The main variant of OpenPGP CA is a CLI tool, designed for use by a CA admin.
  - The functionality can also be integrated into existing systems as a REST service.
- All state is persisted in a single file (using an SQLite database).
  - This makes it easy to reason about backups and to keep the
    CA database safe[^dbsafe].
 
[^dbsafe]: Depending on details of how the instance is set up, the CA database
is often *very* security critical.

# Authentication and the OpenPGP web of trust

The OpenPGP standard provides a powerful mechanism to authenticate keys: the
["web of trust"](https://en.wikipedia.org/wiki/Web_of_trust)[^sequoia-wot].

[^sequoia-wot]: See the `sequoia-wot` project for a detailed
[specification](https://sequoia-pgp.gitlab.io/sequoia-wot/)
of the web of trust mechanism.

The web of trust mechanism reasons about authentication on behalf of the
user, based on *certifications*.
A certification is a machine-readable vouch, which asserts that an identity
(a person, a machine, or an organization) controls some key[^cert-term], or (partially)
delegates authentication decisions to another party.

[^cert-term]: This type of certification is also called "third-party
certification" (in contrast to "self-signatures", which are used
within OpenPGP certificates, e.g. to bind User IDs to one's certificate).

    Colloquially, third-party certifications are sometimes referred to simply
as "signatures" (as in: "Bob signed Carol's key").

These certifications can be exchanged between OpenPGP implementations and
users, and are typically included when distributing OpenPGP keys.

## Regular certifications

Let's first look at a regular certification. Here, Bob certifies the identity
`carol@example.org` on Carol's key (after checking that the key is indeed
controlled by Carol):

{{< figure src="../../book/cert-1.png"
width=75% alt="Bob certifies an identity on Carol's Key" >}}

Specifically, Bob asserts that Carol (or, more precisely: Carol's identity
`carol@example.org`) controls the key `0xCCCC`[^fp].
And, he has created a certification that records that assertion.
Bob's certification binds the identity `carol@example.org` with
the key `0xCCCC`.

[^fp]: We're using identifiers like `0xCCCC` in this document as a way
to reference a specific OpenPGP key. The string `0xCCCC` is intended as
shorthand for the key's OpenPGP fingerprint.
Actual, unabbreviated, OpenPGP fingerprints look like:
`34F9E4B6A0A70BFEC5AE45198356989DF1977575`.
(They are sometimes prefixed with `0x`. We use this notation here to make
"identifiers of keys" easy to distinguish from other identity markers)

This certification is first useful to Bob: his software now
understands that he has verified Carol's key. However, if he publishes the
certification, e.g., on a key server, it can also be useful to others.

## Manually reasoning about certifications

Let's imagine that Alice wants to communicate with Carol,
but she hasn't authenticated Carol's key yet.
If Alice has access to Bob's certification, it serves as evidence that
`0xCCCC` is the right key for Carol:

{{< figure src="../../book/cert-2.png"
width=90% alt="Alice inspects Bob's certification of Carol's Key" >}}

Of course, Alice should only trust Bob's certification as much as she
trusts Bob to correctly certify Carol's key.

## Certifications and delegation

The mechanisms that make up OpenPGP's web of trust are extremely powerful.

Besides certifying that an identity controls a key (like: `carol@example.org`
controls key `0xCCCC`), it is possible to designate a key as a
*trusted introducer*, i.e., a certificate authority (CA)[^delegate].

[^delegate]: Popular implementations of OpenPGP currently only support trust
signatures that *combine* certifying the binding between an identity and a key
with delegation of authentication to that key.

    In this text, we ignore the certification aspect of those trust signatures
because it is not relevant in our current context. Alice's *trust signature* in
practice does two things: it certifies the binding `<bob@example.org, 0xBBBB>`
and it delegates authentication decisions to `0xBBBB`.

In OpenPGP terms, this is done using a type of certification called a
["trust signature"](https://tools.ietf.org/html/rfc4880#section-5.2.3.13)
(sometimes abbreviated as *tsig*)[^ownertrust].

[^ownertrust]: With GnuPG, it's possible to get similar semantics to trust signatures by
setting the ownertrust property for keys. When using GnuPG, if a
user were to set this ownertrust property on the OpenPGP CA key to the
value 'Fully', their setup would work as expected.

    However, OpenPGP CA strongly prefers the use of trust signatures to designate
the OpenPGP CA key as trusted. Trust signatures are specified in the
OpenPGP standard, and as a standardized OpenPGP concept, they work the same
with all OpenPGP implementations.

Continuing the example from above, instead of manually reasoning about Bob's
certification, Alice can designate Bob as a trusted introducer.
This way, Alice delegates (some) authentication decisions to Bob.

{{< figure src="../../book/cert-3.png"
width=90% alt="Alice designates Bob's key as a trusted introducer" >}}

This delegation signals to Alice's OpenPGP software that she wants it to rely
on Bob's certifications as if she had made those same certifications herself.

## Limiting delegation of authentication decisions

OpenPGP's delegation mechanism provides nuance.  It is possible to limit the
scope of trust using
[regular expressions](https://tools.ietf.org/html/rfc4880#section-5.2.3.14) over the User ID.

For instance, Alice may trust Dave from the NSA to certify users within his
own organization, but not other people.

{{< figure src="../../book/cert-3a.png"
width=90% alt="Alice designates Dave's key as a trusted introducer, limited to identities *@nsa.gov" >}}

## Combined effect of certifications

Now let's see how two of the certifications we introduced above interact:

Alice has designated Bob as a trusted introducer, and Bob has
certified the binding between Carol's key `0xCCCC` and Carol's identity
`carol@example.org`.
Based on these two certifications, Alice's OpenPGP software automatically
determines that Carol's key and her identity are legitimately connected.
It considers Carol's key authenticated:

{{< figure src="../../book/cert-4.png"
width=100% alt="Alice's software now considers Carol's key authenticated" >}}

This is an example of how the web of trust mechanism operates: it reasons
about a set of keys and certifications, and determines if the certifications
show that a key (in this example: Carol's) is considered authentic.

# OpenPGP CA uses the web of trust

Our approach to authentication is built using the existing and well-established
OpenPGP "web of trust" mechanisms.
Because these mechanisms are part of the OpenPGP
specification, all OpenPGP implementations understand them. Thus, OpenPGP CA
works out of the box with existing OpenPGP implementations, such as GnuPG.

OpenPGP CA makes it easy for organizations to leverage the power of the OpenPGP
web of trust, by moving the burden of authentication away from users (and to
the new role of the in-house CA admin).

## Authentication within an organization

With OpenPGP CA, the CA certifies the keys of each user in the
organization using regular OpenPGP certifications.
To take advantage of these certifications, users in the organization need
to delegate authentication decisions to their organization's CA.
In OpenPGP terms, users generate a trust signature for the CA's key.

In short, each user normally certifies the CA's key, and the CA certifies
each user's key (the CA asserts that it has verified the link between the
user's email identity and their key).

Consider, for instance, Alice whose email address is `alice@example.org`:

- The CA key certifies the `alice@example.org` User ID on Alice's key.  This
  certification means that the CA has verified that the key is indeed
  controlled by Alice.  
  As a consequence of this certification, anyone who uses
  `openpgp-ca@example.org` as a trusted introducer has an authenticated
  path to Alice's key.
- Alice designates `openpgp-ca@example.org` as a trusted introducer.
  This delegation means that Alice trusts certifications that the CA key
  makes on other keys; Alice trusts the CA to authenticate third parties
  on her behalf.

The following figure shows the keys and certifications in an
OpenPGP CA-using organization with two users, Alice and Bob.

{{< figure src="../../book/oca-certs.png"
width=100% alt="Certifications between Alice, Bob and the CA Key" >}}

The mutual certifications between each user and the CA mean that all
members of the organization have a verified path to each other.  And this
remains the case even as new members join the organization: users don't
need to take any special action to authenticate new colleagues (or new keys
for existing colleagues), only the CA admin does.

For example, if Alice wants to authenticate Bob, her OpenPGP software
will use two certifications to do the authentication: the delegation
Alice made to the CA's key, and the certification that the CA made on Bob's
key. The next figure illustrates how Alice's software automatically
authenticates Bob's key:

{{< figure src="../../book/oca-auth.png"
width=100% alt="Alice authenticates Bob using OpenPGP CA certifications" >}}

## Authentication between organizations: Bridges

Organizations often work closely with a few other organizations.
It greatly simplifies secure communication when users have automatic
authenticated access to keys of colleagues in sister organizations.

As above, our goal is secure communication, without manual overhead.
OpenPGP CA supports this use case by facilitating the creation of so-called
bridges between organizations.

In technical terms, OpenPGP CA bridges organizations using *scoped* trust signatures.

For instance, if users at the two organizations `alpha.example` and `beta.example`
communicate with each other on a regular basis, then the two OpenPGP CA
administrators can exchange fingerprints, and create a trust
signature over each other's domain.    These trust signatures are usually scoped to the domain of the
respective remote organization. The reason for this is that the CA admin of
`alpha.example` probably only wants the CA at `beta.example` to act as a
trusted introducer for users at `beta.example`, but not for keys outside that
namespace (allowing the remote CA admin to act as a trusted introducer for
arbitrary User IDs gives the remote CA admin too much power, in most
contexts).

Using a bridge, members of each organization can authenticate members of
the remote organization.  The following figure shows the keys and
certifications that are created when bridging two organizations.

{{< figure src="../../book/bridge.png"
width=100% alt="Bridge between two organizations" >}}

The bold path shows how Alice's OpenPGP software can authenticate Bob.

## Gateways into organizations

Because users use a trust signature to certify the OpenPGP CA's key,
anyone who considers someone at an OpenPGP CA-using organization to be a
trusted introducer can also automatically authenticate everyone else in the
organization.

## Emergent Properties

Many individual users of OpenPGP don't currently use trusted introducers.
But that doesn't mean that their tools can't make use of such certifications.
Consider the following scenario.
Alice and Bob are from `example.org`, which uses OpenPGP
CA.  And Zed, an individual who is not part of an organization that uses
OpenPGP CA, regularly communicates with Alice.  Now, let's say that Zed
wants to send Bob an email.  Zed might find many keys that claim to be
Bob's.  Some might be keys that Bob has lost control of and hasn't revoked.
Others may have been published by an attacker.  What key should he use for
Bob?

Because we use standard OpenPGP artifacts - in particular trust signatures -
we can publish them. Then, using the normal web of trust mechanisms, Zed's
tools can detect a reasonable key for
Bob based on two observations.  First, the tool can observe that there
is a verified path from Alice to Bob's current key in the web of
trust.  Second, since Zed is able to successfully communicate with
Alice, there is a good chance that he has the correct key for her.
Thus, we have a likely reasonable candidate key for Bob even if it is
not strongly authenticated.

{{< figure src="../../book/oca-zed.png"
width=100% alt="Zed chooses a key for Bob based on an authentication chain from Alice" >}}

This approach is just a heuristic.  But, it is a good
heuristic when strong authentication is not required.
It avoids the frustrating and debilitating scenario in which Zed
uses the wrong key and Bob can't decrypt his message.


# User key setup for OpenPGP CA

There are two different ways in which user keys may be set up in an
organization that uses OpenPGP CA:
decentralized and centralized user key creation.

In both cases, the CA admin makes sure that a structure of
certifications exists between keys in the organization.

## Decentralized user key creation

"Decentralized key creation" is the default approach:

Users [create their own keys](/doc/keys-import/), on their own devices.

After a user has created a new key, they send their new public key to the
CA admin, who, after authenticating it, adds it to the CA database
(the CA software automatically generates certifications for the identities
that the admin has authenticated).

To get all the benefits of OpenPGP CA, the user also needs to
certify the CA's key (to delegate authentication decisions to the CA).
This is a non-trivial task, with today's tooling[^decentral].
But it only needs to be done once per user, as initial setup.
And with help from the CA admin or a help desk team, this is doable.

[^decentral]: In upcoming versions of OpenPGP CA, we will add tooling to make
the user side of this workflow much easier.

## Centralized user key creation

An alternative workflow is that user keys are (centrally)
[created by the OpenPGP CA admin](/doc/keys-create/).
In this workflow - because OpenPGP CA creates the user's key - OpenPGP CA
can automatically generate all required certifications.

During creation of the user key, OpenPGP CA only keeps the private
key material in RAM and never saves it to disk.
Still, this method is more risky than decentralized user key creation.

### Revocation Certificates

During centralized key creation, OpenPGP CA optionally generates and
stores several revocation certificates[^revoc] for users[^des-revoker].

[^revoc]: The reason to create multiple revocations is so that at a later
time, the most appropriate revocation reason can be used.
e.g., key lost, key compromised, etc.

[^des-revoker]: In theory, we could simply have users set the CA key as a designated revoker,
but there isn't much support for designated revokers in the OpenPGP ecosystem.

This allows the CA operator to revoke user keys when users lose access to
them, or when users leave the organization.


# Setup workflows on the user side

## Decentralized workflow

In most contexts, it is preferable to generate user keys on the user's own
machine. This way, the user has sole control over the private key material.

However, with currently available tooling, decentrally setting up users keys
for use with OpenPGP CA is a multi-step process that may require some
guidance:

- The user obtains and authenticates the CA's public key,
- The user generates their own new key,
- and generates a certification (trust signature) for the CA's key.
- This certification and the user's public key are transferred to
  the CA admin.

The CA admin then authenticates and imports this new user key into OpenPGP CA
(which automatically certifies the specified User IDs of the user's key).

## Centralized workflow

Alternatively, with OpenPGP CA, the CA admin can provision a user with a
key. In addition to generating the key, OpenPGP CA creates certifications
between the new user key and the CA's key.  The admin sends the private key
(including a certification for the CA key) to the user[^publish].
This can be done by saving it on a USB drive, or sending it by email
(encrypted with a strong password).

[^publish]: Additionally, the user's public key is optionally published in
the organization's WKD, and uploaded to key servers.

The user then imports their new private key and sets it to
be "ultimately" trusted (i.e., they tell their OpenPGP implementation that this is
their own key), and they import the CA's public key.

Following this, the user has an OpenPGP setup where their OpenPGP software
can automatically find the keys
for their coworkers (via WKD) and strongly authenticate those keys.  The users
don't need to compare fingerprints, they don't need to go to a key signing party, 
in fact, they don't even need to understand what a certification is.

# The OpenPGP CA tool from the admin's perspective

OpenPGP CA as a tool for the CA admin operates on a database that contains
a number of artifacts:
 
- It is a central repository of all
  public OpenPGP keys of the users in the organization.

- It also contains certifications between the users and the CA admin.

- Further, OpenPGP CA's database optionally contains
  [revocation certificates](#revocation-certificates) for user keys.

- Finally, the CA's private OpenPGP key is in the database, if the CA is
  operated in "software key"-mode[^card]. The private key is used
  to generate the certifications by the CA, and as such needs to be kept safe.

[^card]: When using the OpenPGP card backend, the CA's private key is not
stored in the CA database, but resides on the hardware device.
Using a hardware-backed CA key makes it easier to keep the CA private key
material safe.

Currently, OpenPGP CA can export OpenPGP keys, create a WKD and
*GPG Sync*-style
[Keylists](https://datatracker.ietf.org/doc/draft-mccain-keylist/).
The admin needs to publish these manually[^publish2].

[^publish2]: For an upcoming version of OpenPGP CA, we plan to add support
to more easily publish user's public keys on key servers and via other
distribution mechanisms.