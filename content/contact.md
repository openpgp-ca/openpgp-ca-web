+++
title = "Contact us"
keywords = ["OpenPGP CA", "contact"]
+++

<!--
SPDX-FileCopyrightText: 2019-2023 Heiko Schaefer <heiko@schaefer.name>
SPDX-License-Identifier: GPL-3.0-or-later
-->

Come join us for any questions, feedback, or support!

We're on:
- [Matrix](https://matrix.to/#/#openpgp-ca:matrix.org)
- The OFTC IRC network in `#openpgp-ca`

You can contact [Heiko via email](mailto:heiko@schaefer.name)
([PGP key](https://openpgp-ca.org/heiko.asc)).
